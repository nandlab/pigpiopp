#ifndef PIGPIOPP_EXCEPTION_H
#define PIGPIOPP_EXCEPTION_H

#include <exception>

namespace pigpiopp {

class exception : public std::exception
{
    int errcode;
public:
    exception(int errcode);
    int code() const noexcept;
    const char *what() const noexcept override;
};

}

#endif // PIGPIOPP_EXCEPTION_H
